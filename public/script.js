let batu = document.getElementById("batu");
let gunting = document.getElementById("gunting");
let kertas = document.getElementById("kertas");
let checkWin = document.getElementById("anouncement")
let character = ["batu", "gunting", "kertas"];
let refresh = document.getElementById("refresh");

function reset(){
    gunting.classList.remove('baru');
    batu.classList.remove('baru');
    kertas.classList.remove('baru');
    versus();

    character.forEach((comp) => {
        let compElement = document.getElementById(`${comp}-com`);
             compElement.classList.remove('baru');
     });
}

refresh.addEventListener('click', function(){
    reset();
})


batu.addEventListener ('click', function(e){
    batu.classList.add('baru');
    kertas.classList.remove('baru');
    gunting.classList.remove('baru');
    versus('batu');
})

gunting.addEventListener ('click', function(e){
    gunting.classList.add('baru');
    batu.classList.remove('baru');
    kertas.classList.remove('baru');
    versus('gunting');
})

kertas.addEventListener ('click', function(e){
    kertas.classList.add('baru');
    batu.classList.remove('baru');
    gunting.classList.remove('baru');
    versus('kertas');
})

function versus(choice){
    let player = choice;

    let character = ["batu", "gunting", "kertas"];
    let randomNumber = Math.floor(Math.random() *3)
    let comp = character[randomNumber];

    character.forEach((comp) => {
       let compElement = document.getElementById(`${comp}-com`);
            compElement.classList.remove('baru');
    });

    let compSign = document.getElementById(`${comp}-com`);
    compSign.classList.add('baru');

    let result = winner(choice, comp);
    checkWin.textContent = result;
}


function winner(player, comp){
    if(player === comp) return 'draw';

    if (player === 'batu' && comp === 'gunting'){
        return 'player win'
    } else if(player === 'batu' && comp === 'kertas'){
        return 'comp win'
    } else if (player === 'gunting' && comp === 'kertas'){
        return 'player win'
    }else if (player === 'gunting' && comp === 'batu'){
        return 'comp win'
    }else if (player === 'kertas' && comp === 'gunting'){
        return 'comp win'
    }else if (player === 'kertas' && comp === 'batu'){
        return 'player win'
    }

}
